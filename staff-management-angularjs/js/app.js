let app = angular.module('staffManagement', ['ngRoute','addCtrlModule','listControllerModule']);
app.controller('AppController', ['$scope', function ($scope){
    $scope.title = 'Staff Management';
}]);
app.config(($routeProvider) => {
    $routeProvider
    .when("/", {
        templateUrl: "/views/home.html",
        controller: "AppController"
    })
    .when("/add",{
        templateUrl: "/views/add.html",
        controller: "AddController"
    }).
    when("/list",{
        templateUrl: "/views/list.html",
        controller: "ListController"
    })
});




    // .when('/add',{
    //     templateUrl:'views/add.html',
    //     controller:'AddController'
    // }).
    // otherwise({
    //     redirectTo: '/'
    // });


