const fs = require("fs");
const bodyParser = require('body-parser')

const EmployeeModel = require('../models/employee');


module.exports.addEmployee = (req, res) => {
    const employeeInfo = req.body;
    console.log(employeeInfo);
    EmployeeModel.create(employeeInfo, (err, data) => {
        if(err){
            res.json({
                "status": "500",
                "message": "Failed to add item"
            });
        }
        else {
       res.json({
           "status" : "200",
           "message": "OK"
       });
     }
       
    });
};

