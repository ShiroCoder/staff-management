const EmployeeModel = require('../models/employee');


module.exports.updateEmployee = (req, res) => {
    const employeeInfo = req.body.data;

    const emplCode = employeeInfo.eCode;
    console.log(employeeInfo, emplCode);
    EmployeeModel.findOneAndUpdate({eCode:emplCode}, {$set:employeeInfo}, {upsert:true}, (err, data) => {
        if(err){
            res.json({
                "status": "500",
                "message": "Failed to update item"
            });
        }
        else {
       res.json({
           "status" : "200",
           "message": "OK"
       });
     }
       
    });
};
