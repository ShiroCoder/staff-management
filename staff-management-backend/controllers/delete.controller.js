const EmployeeModel = require('../models/employee');


module.exports.deleteEmployee = (req, res) => {
    const eCode = req.params.eCode;
    EmployeeModel.findOneAndDelete({eCode:eCode}, (err, data) => {
        if(err){
            res.json({
                "status": "500",
                "message": "Failed to delete item"
            });
        }
        else {
       res.json({
           "status" : "200",
           "message": "OK"
       });
     }
       
    });
};
