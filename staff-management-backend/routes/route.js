const bodyParser = require('body-parser');

const jsonParser = bodyParser.json();
module.exports = (app) => {
    
const addController = require('../controllers/add.controller');
const updateController = require('../controllers/update.controller');
const deleteController = require('../controllers/delete.controller');
const getController = require('../controllers/get.controller');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/getEmpls', jsonParser, getController.getEmployee);
app.post('/addEmp', jsonParser, addController.addEmployee);
app.patch('/updateEmp/:eCode', jsonParser, updateController.updateEmployee);
app.delete('/deleteEmp/:eCode', jsonParser, deleteController.deleteEmployee);
}