const mongoose = require('mongoose');

let employeeSchema = new mongoose.Schema(
    {
        eCode: {
            type: String,
            trim: true,
            required: true,
            unique:true
        },
        eName: {
            type: String,
            trim: true,
            required: true
        },
        gender: String,
        address: String,
        status: {
            type: String
        }
    }
);

module.exports = mongoose.model('Employee', employeeSchema);